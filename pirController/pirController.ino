#include <OSCMessage.h>
#include <EEPROM.h>

#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SPI.h>    
#include <OSCMessage.h>

#define PIN_PID 2
const int bubbleTime = 5000; // number ms for bubbles to be on after PID sensor is activates
long int time = 0; // millis() when PID sensor activated
boolean bubbling = false;

byte IP_LAST = EEPROM.read(0);

EthernetUDP Udp;

//the Arduino's IP
IPAddress ip(192, 168, 0, IP_LAST);
//destination IP
IPAddress outIpOne(192, 168, 0, 1);
IPAddress outIpTwo(192, 198, 0, 2);

const unsigned int outPort = 9999;

byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, IP_LAST }; // you can find this written on the board of some Arduino Ethernets or shields



void setup() {
  Ethernet.begin(mac,ip);
  Udp.begin(9999);

}


void loop(){

  if(digitalRead(PIN_PID) == LOW){
    // send start message when PID is low
    
    OSCMessage msg("/mots/bubbles/start");
    msg.add((int32_t)1);
   
    Udp.beginPacket(outIpOne, outPort);
    msg.send(Udp); // send the bytes to the SLIP stream
    Udp.endPacket(); // mark the end of the OSC Packet
    
    Udp.beginPacket(outIpTwo, outPort);
    msg.send(Udp); // send the bytes to the SLIP stream
    Udp.endPacket(); // mark the end of the OSC Packet
    
    msg.empty(); // free space occupied by message
    
    time = millis();
    bubbling = true;
  }

if(bubbling == true){
  // check to see if bubbling to avoid sending unnecessary stop messages
  if(millis() - time > bubbleTime){
    // send off message if time elapsed since PIR last activated exceddes the designated time
    OSCMessage msg("/mots/bubbles/stop");
    msg.add((int32_t)1);
   
    Udp.beginPacket(outIpOne, outPort);
    msg.send(Udp); // send the bytes to the SLIP stream
    Udp.endPacket(); // mark the end of the OSC Packet
    
    Udp.beginPacket(outIpTwo, outPort);
    msg.send(Udp); // send the bytes to the SLIP stream
    Udp.endPacket(); // mark the end of the OSC Packet
    
    msg.empty(); // free space occupied by message
    
    bubbling = false;
  }
}

}

