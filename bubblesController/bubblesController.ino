
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SPI.h>    
#include <EEPROM.h>

#include <OSCBundle.h>
#include <OSCBoards.h>

#define PIN_FLOAT 7 // 5V goes to switch on center pin, 7 pulled low with 10k resistor
#define PIN_PUMP 6 // connected to base of 2n2222a switching 12V from Vin to pump
#define PIN_BUBBLE 5 // Vin connected to RCA shield, pin 5 connected to center pin through 120R resistor. HIGH = on, LOW = off

const int refillTime = 5000; // number of ms for pump to remain on after level switch has closed
long int timerPump = 0; // timer fot bubble refilling;

int bubbling = false;

byte IP_LAST = EEPROM.read(0);

EthernetUDP Udp;
byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, IP_LAST }; // you can find this written on the board of some Arduino Ethernets or shields



//the Arduino's IP
IPAddress ip(10, 0, 0, IP_LAST);

//port numbers
const unsigned int inPort = 9999;

void setup() {
  pinMode(PIN_PUMP, OUTPUT);
  digitalWrite(PIN_PUMP, LOW);
  pinMode(PIN_BUBBLE, OUTPUT);
  digitalWrite(PIN_BUBBLE, LOW);
  pinMode(PIN_FLOAT, INPUT);
  
  //setup ethernet part
  Ethernet.begin(mac,ip);
  Udp.begin(inPort);

}

//reads and dispatches the incoming message
void loop(){ 
    OSCMessage bundleIN;
   int size;
 
   if( (size = Udp.parsePacket())>0)
   {
     while(size--)
       bundleIN.fill(Udp.read());

      if(!bundleIN.hasError())
        bundleIN.route("/mots/bubbles/start", bubbleStart);
        bundleIN.route("/mots/bubbles/stop", bubbleStop);
   }
   
   // if level switch it closed (low level) turn on pump an set timer
   if(digitalRead(PIN_FLOAT) == HIGH){
     timerPump = millis();
     digitalWrite(PIN_PUMP, HIGH);
   }
   
   // leave on pump till desired refill time is reached
   if((millis()-timerPump)>refillTime){
    digitalWrite(PIN_PUMP, LOW); 
   }
}

void bubbleStart(OSCMessage &msg, int num ){
 digitalWrite(PIN_BUBBLE, HIGH); 
}

void bubbleStop(OSCMessage &msg, int num){
 digitalWrite(PIN_BUBBLE, LOW); 
}
